
// Global variables defined to provide dynamic script loading as well as
// module declaration and use via dependency requirements.
//
var relyjs, define, require;

(function(global) {

  var RelyJS = function() {

    var self = this;

  };

  RelyJS.prototype = (function(config) {

    var

    //
    // Private Variables
    //

    _definitions = {},
    _instances = {},
    _instantiating = [],
    _waitSeconds = 15,

    //
    // Private Functions
    //

    // Returns true if the specified parameters is a function or false otherwise.
    //
    _isFunction = function(obj) {
      return Object.prototype.toString.call(obj) === '[object Function]';
    },

    // Returns true if the specified parameter is an array or false otherwise.
    //
    _isArray = function(obj) {
      return Object.prototype.toString.call(obj) === '[object Array]';
    },

    // Loads a JavaScript file and calls the specified callback once it has been loaded.
    //
    _loadScript = function(src, callback) {
      var ready = false;
      var scriptElem = document.createElement('script');
      scriptElem.src = src;
      scriptElem.onload = scriptElem.onreadystatechange = function() {
        if( (scriptElem.readyState && scriptElem.readyState !== 'complete' && scriptElem.readyState !== 'loaded') || ready) {
          return false;
        }
        scriptElem.onload = scriptElem.onreadystatechange = null;
        ready = true;
        if( callback ) {
          callback();
        }
      };
      document.body.appendChild(scriptElem);
    };

    //
    // Public Interface
    //

    return {

      constructor: RelyJS,

      // Configures RelyJS. Configuration options include:
      //
      // externals:
      //    An object containing dependency name to object mappings. Each
      //    property of the object makes the provided object available as
      //    a dependency via a call to require. For example:
      //
      //      jquery: $
      //
      //    will make the $ global available as a dependency via a call
      //    to require('jquery'). This allows non-modular third-party libraries
      //    that are not declared using define to still be required as
      //    dependencies.
      //
      configure: function(config) {

        var self = this;

        for(var name in config.externals) {
          _instances[name] = config.externals[name];
        }

      },

      // Returns an instance of the specified dependency. If the module with the
      // specified name has not already been instantiated, it will be instantiated
      // as a part of this call. All child dependencies will also be instantiated
      // as appropriate. Performs checks for circular dependencies and will throw
      // an error if a circular dependency is found or an undefined dependency is
      // encountered.
      //
      require: function(dependencyName) {

        var self = this;

        // Check for circular dependencies
        if(_instantiating.indexOf(dependencyName) >= 0) {
          throw new Error('Circular Dependency: ' + _instantiating.join(' -> ') + ' -> ' + dependencyName);
        }

        // Check whether or not the dependency has already been instantiated
        if(typeof _instances[dependencyName] === 'undefined') {

          var fn = _definitions[dependencyName];

          // Check whether or not the dependency has been defined with a call to define
          if(typeof fn === 'undefined' || !_isFunction(fn)) {
            throw new Error('Undefined Dependency: ' + dependencyName);
          }

          // Instantiate the referenced dependency while maintaining an instantiation
          // stack for detecting circular dependencies
          _instantiating.push(dependencyName);
          _instances[dependencyName] = fn(global.require);
          _instantiating.pop();

        }

        return _instances[dependencyName];
      },

      // Defines a module that may be used with a call to require.
      //
      define: function(name, definition) {

        var self = this;

        _definitions[name] = definition;

      },

      // Loads one or more JavaScript files and calls the provided callback function
      // once all scripts are loaded. The first parameter may be a string with the
      // URL of a single script file to load or an array of strings containing a
      // URL for each script to load. Scripts are not guaranteed to load in any
      // particular order but all scripts are guaranteed to be loaded before the
      // callback function is called.
      //
      load: function(sources, callback) {

        var self = this;

        // Load multiple scripts
        if(_isArray(sources)) {

          var remainingCount = sources.length;

          // Set a wait timer to error on script loading timeout
          var waitTimer = setTimeout(function() {
            if(remainingCount > 0) {
              throw new Error('RelyJS Timeout: ' + sources);
            }
          }, _waitSeconds * 1000);

          var singleLoadCallback = function() {
            remainingCount--;
            if(remainingCount < 1) {
              // all scripts have loaded
              clearTimeout(waitTimer);
              callback();
            }
          };

          for(var i = 0; i < sources.length; i++) {
            _loadScript(sources[i], singleLoadCallback);
          }

        }

        // Load a single script
        else {
          _loadScript(sources, callback);
        }

      }
    };

  })();

  // Export globals if they do not already exist.
  //
  if(!global.relyjs) {

    // Create RelyJS instance
    global.relyjs = new RelyJS();

    // Proxy global require calls to execute in the context of the relyjs object.
    global.require = function(dependencyName) {
      return global.relyjs.require(dependencyName);
    };

    // Proxy global define calls to execute in the context of the relyjs object.
    global.define = function(name, definition) {
      global.relyjs.define(name, definition);
    };

  }

}(this));